package file.handling;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileUtils {

    public static List<List<Double>> read(String path) {
        List<List<Double>> cleanedData = new ArrayList<>();
        String dataStream;

        try{
            File file = new File(path);
            FileReader data = new FileReader(file);

            BufferedReader buffData = new BufferedReader(data);

            if(file.exists()){
                dataStream = buffData.readLine();
                String[] parsedDatastr;

                while(dataStream != null){
                    parsedDatastr = dataStream.split(";");

                    String[] removeFirstData = Arrays.copyOfRange(parsedDatastr, 1, parsedDatastr.length);
                    Double[] doubleConverted = Arrays.stream(removeFirstData).map(Double::valueOf).toArray(Double[]::new);
                    cleanedData.add(Arrays.asList(doubleConverted));
                    dataStream = buffData.readLine();
                }

                buffData.close();

            }

        }catch (IOException ioe){
            ioe.printStackTrace();
        }

        return cleanedData;
    }
}
