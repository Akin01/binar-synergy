package statistic;

import java.util.*;

public class Utils {
    static List<Double> data;
    static int data_length;

    public Utils(List<Double> data){
        Utils.data = data;
        Utils.data_length = data.size();
    }

    public Double mean(){
        Double sum_of_data = 0.0;

        for(Double i: data){
            sum_of_data += i;
        }

        return sum_of_data / data_length;

    }

    public Double median(){
        int middle = data_length / 2;

        if(data_length % 2 != 0){
            return data.get(middle + 1);
        }else{
            return (data.get(middle) + data.get(middle + 1)) / 2;
        }
    }

    public List<Double> modus(){
        Map<Double, Integer> data_dist = new HashMap<>();

        for(Double i: data){
            if(data_dist.isEmpty()){
                data_dist.put(i, 1);
            }else{
                if(data_dist.containsKey(i)){
                    data_dist.put(i, data_dist.get(i) + 1);
                }else{
                    data_dist.put(i, 1);
                }
            }
        }

        Integer max_value = Collections.max(data_dist.values());
        List<Double> mode = new ArrayList<>();

        for(Map.Entry<Double, Integer> set: data_dist.entrySet()){
            if(set.getValue().equals(max_value)){
                mode.add(set.getKey());
            }
        }

        return mode;
    }

    public static List<Double> flatArrayList2D(List<List<Double>> data){
        List<Double> result = new ArrayList<>();

        for(List<Double> i: data){
            result.addAll(i);
        }

        return result;
    }
}
