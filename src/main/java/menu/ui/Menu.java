package menu.ui;


public class Menu {
    public static String[] mainMenuList  = {
            "Generate TXT untuk menampilkan modus",
            "Generate TXT untuk menampilkan rata-rata, median",
            "Generate kedua file",
            "Exit"
    };

    static String title = "Aplikasi Pengolah Nilai Siswa";

    public static void mainMenu(String inputFolder){
        titleTemplate(title);
        System.out.printf("Letakkan file csv dengan nama file data_sekolah di direktori berikut %s\n\n", inputFolder);
        System.out.println("pilih menu:");
        displayMenu(Menu.mainMenuList);
    }

    public static void resultMenu(String ouputFolder){
        titleTemplate(title);
        System.out.printf("File telah digenerate di %s\n", ouputFolder);
        System.out.println("Silahkan cek!\n");
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke menu utama");
    }

    public static void fileNotFound(){
        titleTemplate(title);
        System.out.println("File tidak ditemukan!\n");
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke menu utama");
    }

    private static void displayMenu(String[] menu){
        for(int a = 0; a < menu.length; a++){
            if(a == (menu.length - 1)){
                System.out.println("0. " + menu[a]);
            }else {
                System.out.println((a+1) + ". " + menu[a]);
            }
        }
    }

    private static void titleTemplate(String title){
        System.out.println(underline(title.length()));
        System.out.println(title);
        System.out.println(underline(title.length()));
    }

    private static String underline(int len){
        StringBuilder line = new StringBuilder();
        for(int i = 0; i < len + 3; i++){
            line.append("-");
        }
        return line.toString();
    }
}
