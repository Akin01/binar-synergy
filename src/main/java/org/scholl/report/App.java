package org.scholl.report;
import menu.ui.Menu;
import statistic.Utils;
import file.handling.FileUtils;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class App 
{
    public static void main( String[] args ) {
        String parent = new File("").getAbsolutePath();
        Scanner input = new Scanner(System.in);

        Path folderInput = Paths.get(parent, "src", "data", "input");
        Path folderOutput = Paths.get(parent, "src", "data", "output");

        File readFolder = new File(folderInput.toString());

        String[] file_list = readFolder.list();

        List<List<Double>> gettedData = new ArrayList<>();

        assert file_list != null;
        for(String i: file_list){
            if(i != null){
                Path fileName = Paths.get(folderInput.toString(), i);
                gettedData = FileUtils.read(fileName.toString());
            }
        }

        Utils statistic = new Utils(Utils.flatArrayList2D(gettedData));


        while (true){
            Menu.mainMenu(folderInput.toString());

            System.out.print("\nEnter your option: ");
            int pilihan = input.nextInt();

            if (pilihan >= 1 && pilihan <= Menu.mainMenuList.length -1){
                if(pilihan == 1){
                    Menu.resultMenu(folderOutput.toString());
                    System.out.printf("Modus: %s\n", statistic.modus().toString());
                } else if (pilihan == 2) {
                    Menu.resultMenu(folderOutput.toString());
                    System.out.printf("Mean: %.2f\n", statistic.mean());
                    System.out.printf("Median: %.2f\n", statistic.median());
                } else if (pilihan == 3) {
                    Menu.resultMenu(folderOutput.toString());
                    System.out.printf("Modus: %s\n", statistic.modus().toString());
                    System.out.printf("Mean: %.2f\n", statistic.mean());
                    System.out.printf("Median: %.2f\n", statistic.median());
                }
            }else if (pilihan == 0){
                break;
            }
            else {
                System.out.println("\nPilihan anda tidak ada.\nSilahkan pilih kembali.\n");
            }
        }
    }

    public static List<Double> generateRandomDoubleList(int length){
        List<Double> data = new ArrayList<>();
        Random random = new Random();

        for(int i = 0; i < length; i++) data.add(random.nextDouble());

        return data;
    }
}
